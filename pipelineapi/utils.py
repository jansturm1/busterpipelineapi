import nltk
import numpy as np


class TextChunker:
    def __init__(self, alpha=0.5):
        self.alpha = alpha

        try:
            nltk.data.find('tokenizers/punkt')
        except LookupError:
            nltk.download('punkt')

    def chunk_sliding_window(self, chunks: list[str], n: int, m: int, as_str_no_wgts:bool) -> list[str]:
        sentences = [nltk.sent_tokenize(str(chunk)) for chunk in chunks]
        new_chunks = []

        for i in range(len(sentences)):
            new_chunk = []
            
            j = i
            n_remaining = n
            idx_tmp = -1

            while j > 0 and n_remaining > 0:
                if idx_tmp == -1:
                    j-=1
                    idx_tmp = len(sentences[j])-1
                else:
                    idx_tmp-=1
                    
                new_chunk = [sentences[j][idx_tmp]] + new_chunk
                n_remaining-=1
    
            # Add senteces of current chunk
            new_chunk.extend(sentences[i])


            j = i
            m_remaining = m
            idx_tmp = 99999999999

            # print(f"sentences.len={len(sentences)}")

            while j < len(sentences)-1 and m_remaining > 0:
                # print(f"start:while, j={j}, idx_tmp={idx_tmp}, m_remaining={m_remaining}")
        
                if idx_tmp >= len(sentences[j])-1:
                    idx_tmp = 0
                    j+=1

                else:
                    idx_tmp+=1


                # print(f"j={j}, idx_tmp={idx_tmp}, sentences[j].len={len(sentences[j])}")

                new_chunk = new_chunk + [sentences[j][idx_tmp]]
                m_remaining-=1  



            n_prior = n-n_remaining


            # Calculate weights for the sentences in the new chunk
            weights = self._calculate_weights(new_chunk, n_prior, len(sentences[i]))

            if as_str_no_wgts:
                new_chunk = ' '.join(sentence for sentence, weight in zip(new_chunk, weights))
            else:
                new_chunk = [{'sentence':sentence, 'wgt': float(f'{weight:.2f}')} for sentence, weight in zip(new_chunk, weights)]

            new_chunks.append(new_chunk)

        return new_chunks


    def _calculate_weights(self, chunk: list[str], num_prior_sentences: int, current_chunk_len: int) -> np.array:
        weights = np.ones(len(chunk))

        # For sentences from prior chunks
        for sentence_index in range(num_prior_sentences):
            weights[num_prior_sentences - sentence_index - 1] = self.alpha ** (sentence_index + 1)

        # For sentences from the next chunks
        start_index_next_chunk = num_prior_sentences + current_chunk_len
        num_next_sentences = len(chunk) - start_index_next_chunk
        for sentence_index in range(num_next_sentences):
            weights[start_index_next_chunk + sentence_index] = self.alpha ** (sentence_index + 1)

        return weights
