import logging, torch
import pandas as pd
from pipelineapi.utils import TextChunker
from dataclasses import dataclass, field
from pipelineapi.backend import BackendClient
from pipelineapi.zeroshotclassifier import ZeroShotClassifier, ZeroShotClassifierService


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)



def get_device():
    if torch.cuda.is_available():
        return 0 # Use the first GPU if available
    else:
        return -1 # Use CPU if no GPU is available

@dataclass
class PipelineConfig:
    """Pipeline init config"""

    backend_cfg: dict = field(
        default_factory = lambda: {
            "username" : "test",
            "password": "pwd",
            "session_expiration_minutes": 60,
            "session_cache_name" : "cache",
            "base_url" : "http://connection.ijs.si"
        }
    )

    zeroshot_classifier_cfg: dict = field(
        default_factory = lambda: {
            "device" : get_device(),
            "model_task": "zero-shot-classification",
            "model_name": "MoritzLaurer/DeBERTa-v3-base-mnli-fever-anli",
            "classes": lambda: [
                "bibliographic reference",
                "not bibliographic reference",
            ],
        }
    )



class PipelineAPI:

    def __init__(self, backend_client: BackendClient, zeroshot_classifier: ZeroShotClassifier):
        self.backend_client = backend_client
        self.zeroshot_classifier = zeroshot_classifier

    def get_datasources_list(self) -> any:
        """
        Get list of available datasources
        """
        return self.backend_client.get_datasources_list()

    def get_chunks(self, datasource:str) -> pd.DataFrame:
        """
        Get initial chunks- chunks without sliding window and cleaned references
        """
        chunks = self.backend_client.get_chunks(datasource)
        return chunks
    
    
    def create_weighted_chunks(self, chunks:pd.DataFrame, n:int, m:int, as_str_no_wgts:bool) -> pd.DataFrame:

        if chunks.shape[0] > 0:
            textchunker = TextChunker()
            chunks_sliding_window = chunks.copy()
            chunks_sliding_window['content'] = textchunker.chunk_sliding_window(chunks['content'].tolist(), n, m, as_str_no_wgts)
            return chunks_sliding_window
        
        return None
    


    def classify_as_reference(self, chunks: list[str], classes:list[any], heuristic_classes:list[bool]=[], use_heuristic:bool=True):
        scores, labels = self.zeroshot_classifier.classify(chunks, classes, heuristic_classes, use_heuristic)
        return (scores, labels)

