import pandas as pd
from abc import ABC, abstractmethod

class BackendClient(ABC):

    def __init__(self, username:str, password:str, base_url:str, **kwargs):
        self.username = username
        self.password = password
        self.base_url = base_url

    @abstractmethod
    def get_datasources_list(self) -> any:
        """
        Get list of available datasources
        """

    @abstractmethod
    def get_chunks(self, datasource:str) -> pd.DataFrame:
        """
        Get chunks for given datasource
        """

    
   