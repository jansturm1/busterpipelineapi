import requests_cache
import pandas as pd
from io import StringIO
from datetime import timedelta
from pipelineapi.backend.base import BackendClient
import urllib.parse


class BackendClientService(BackendClient):

   
    def __init__(self, session_expiration_minutes: int, session_cache_name:str, **kwargs):
        super().__init__(**kwargs)      
        self.session_expiration_minutes = session_expiration_minutes 
        self.session_cache_name = session_cache_name
        
        self._create_session()


    def _create_session(self):
        """
        Create session for faster cached responses
        """
        self.session = requests_cache.CachedSession(
            self.session_cache_name,
            cache_control=False,
            expire_after=timedelta(minutes=self.session_expiration_minutes),
        )


    def get_datasources_list(self) -> any:
        response = self.session.get(f'{self.base_url}/datasources')
        print(f"response={response}")
        return response.json()


    def get_chunks(self, datasource:str):
        params_str = f"datasource={urllib.parse.quote(datasource)}"
        url = f"{self.base_url}/chunks?{params_str}"
        print(f"url={url}")

        response = self.session.get(url, params=params_str)

        if response.ok and response.text != "null":
            # chunks = pd.read_csv(url, encoding='utf-8', sep='\t')
            chunks = pd.read_csv(StringIO(response.text), encoding='utf-8', sep='\t')
            print(f"chunks.shape={chunks.shape}")
            return chunks
        
        elif response.text == None or response.text == "null":
            raise Exception("Provide correct datasource! Use method: get_datasources_list")
        
        else:
            raise Exception(f"Problem when calling backend!, response={response}")
