from abc import ABC, abstractmethod
from dataclasses import dataclass, field


class ZeroShotClassifier(ABC):
    
    classifier:any = None

    def __init__(self, device:str, model_task:str, model_name:str, classes: list[str]):
        
        print(f"device={device}, model_task={model_task}, model_name={model_name}, classes={classes}")

        self.device = device
        self.model_task = model_task
        self.model_name = model_name
        self.classes = classes


    def _init_classifier():
        """
        Init zero-shot classifier
        """


    @abstractmethod
    def classify(self, chunks: list[str], classes: list[any], heuristic_classes:list[bool], use_heuristic:bool):
        """
        Classify chunks for given list of classes
        """
