from transformers import pipeline
from pipelineapi.zeroshotclassifier.base import ZeroShotClassifier
from timeit import default_timer as timer


class ZeroShotClassifierService(ZeroShotClassifier):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._init_pipeline()


    def _init_pipeline(self):
        self.classifier = pipeline(self.model_task, model=self.model_name, device=self.device)
        print(f"Running on device: {self.classifier.model.device}")

    def classify(self, chunks: list[str], classes: list[any], heuristic_classes:list[bool], use_heuristic:bool):
        start = timer()
        print(f"Start: classify, chunks.len={len(chunks)}, classes.len={len(classes)}, heuristic_classes.len={len(heuristic_classes)}, use_heuristic={use_heuristic}")
        heuristic_used = use_heuristic and len(heuristic_classes) > 0 and len(heuristic_classes) == len(chunks)
        print(f"heuristic_used={heuristic_used}")
        chunks_to_classify = None

        if heuristic_used:
            chunks_to_classify = [chunk for chunk, heuristic_class in zip(chunks, heuristic_classes) if heuristic_class]
        else:
            chunks_to_classify = chunks

        print(f"chunks_to_classify.len={len(chunks_to_classify)}")
        classes_dict = {c['name']:c['mapping'] for c in classes}
        class_names = [c['name'] for c in classes]
        # Process the chunks in batch
        output = self.classifier(chunks_to_classify, class_names, multi_label=False)        
        labels = []
        scores = []
        output_iter = 0

        for i in range(len(chunks)):
            heuristic_class = heuristic_classes[i] if heuristic_used else None
            # Chunks were classified only if heuristic function identified chunk as possible reference, or if heuristic is disabled
            if (heuristic_used and heuristic_class) or not heuristic_used:
                max_label, max_score = self.get_label_score_for_result(output[output_iter], classes_dict)
                labels.append(max_label)
                scores.append(max_score)
                output_iter+=1
            elif heuristic_used and heuristic_class == False:
                labels.append(heuristic_class)
                scores.append(1.0) # Hardcoded value- it can be changed to something else
        
        end = timer()
        print(f"End: classify, time={end-start:.2f}s")
        return (labels, scores)
    

    def get_label_score_for_result(self, result:any, classes_dict:dict):
        max_label, max_score = max(zip(result['labels'], result['scores']), key=lambda x: x[1])
        return classes_dict[max_label], max_score
